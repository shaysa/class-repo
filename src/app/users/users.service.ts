import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map'; // import for map function 
import 'rxjs/add/operator/delay';// import for delay function to make the data delay in
import {AngularFire} from 'angularfire2';
@Injectable()
export class UsersService {
//we chang the privte tp get data from thr FB 
      // private _url = 'http://jsonplaceholder.typicode.com/users';

   usersObsrvable;

  getUsers(){
  //  return this._http.get(this._url).map(res => res.json()).delay(2000)  // res is just the name, like result  

  //we added through the constroctor to bilde a user obyect. to add a spesific user we will right /users/1 all names from firebase
  this.usersObsrvable = this.af.database.list('/users');
  return this.usersObsrvable;
  }

  addUser(user){
     this.usersObsrvable.push(user);  //add the new user to FB

     }

     updateUser(user){
       
     let userKey = user.$key;
     let userData = {name:user.name,email:user.email};
     this.af.database.object('/users/'+userKey).update(userData);

     }


  constructor(private af:AngularFire) { }

}